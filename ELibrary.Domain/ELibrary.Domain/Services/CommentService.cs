﻿using ELibrary.Domain.Abstractions.Repositories;
using ELibrary.Domain.Abstractions.Services;
using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ELibrary.Domain.Services
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository repository)
        {
            _commentRepository = repository;
        }

        public Task<IReadOnlyCollection<Comment>> GetAllByBookAsync(Guid commentId)
        {
            return _commentRepository.GetAllByBookAsync(commentId);
        }

        public Task<Comment> GetBookByIdAsync(Guid commentId)
        {
            return _commentRepository.GetByIdAsync(commentId);
        }

        public Task<Comment> CreateAsync(Comment commentModel)
        {
            return _commentRepository.CreateCommentAsync(commentModel);
        }

        public Task<Comment> DeleteAsync(Guid commentId)
        {
            return _commentRepository.DeleteCommentAsync(commentId);
        }
    }
}
