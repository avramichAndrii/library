﻿using ELibrary.Domain.Abstractions;
using ELibrary.Domain.Abstractions.Services;
using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ELibrary.Domain.Services
{
    class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        public AuthorService(IAuthorRepository authorRepository)
        {
            _authorRepository = authorRepository;
        }

        public Task<IReadOnlyCollection<Author>> GetAllAsync()
        {
            return _authorRepository.GetAllAsync();
        }

        public Task<Author> GetAuthorByIdAsync(Guid authorId)
        {
            return _authorRepository.GetAuthorByIdAsync(authorId);
        }

        public Task<Author> CreateAsync(Author authorModel)
        {
            return _authorRepository.CreateAuthorAsync(authorModel);
        }

        public Task<Author> UpdateAsync(Guid authorId, Author authorModel)
        {
            return _authorRepository.UpdateAuthorAsync(authorId, authorModel);
        }

        public Task<Author> DeleteAsync(Guid authorId)
        {
            return _authorRepository.DeleteAuthorAsync(authorId);
        }
    }

}
