﻿using ELibrary.Domain.Abstractions.Repositories;
using ELibrary.Domain.Abstractions.Services;
using ELibrary.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ELibrary.Domain.Services
{
    public class StoredBookService : IStoredBookService
    {
        private readonly IStoredBookRepository _storedBookRepository;
        public StoredBookService(IStoredBookRepository storedBookRepository)
        {
            _storedBookRepository = storedBookRepository;
        }

        public Task<StoredBook> GetByIdAsync(Guid bookId)
        {
            return _storedBookRepository.GetBookByIdAsync(bookId);
        }

        public Task<StoredBook> CreateAsync(StoredBook bookModel)
        {
            return _storedBookRepository.CreateStoredBookAsync(bookModel);
        }
    }
}
