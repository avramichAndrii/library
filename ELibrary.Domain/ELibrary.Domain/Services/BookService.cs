﻿using ELibrary.Domain.Abstractions;
using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Domain.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;
        public BookService(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public Task<IReadOnlyCollection<Book>> GetAllAsync()
        {
            return _bookRepository.GetAllAsync();
        }

        public Task<Book> GetBookByIdAsync(Guid bookId)
        {
            return _bookRepository.GetBooksByIdAsync(bookId);
        }

        public Task<Book> GetBookByName(string name)
        {
            return _bookRepository.GetBookByNameAsync(name);
        }

        public Task<Book> CreateAsync(Book bookModel)
        {
            return _bookRepository.CreateBookAsync(bookModel);
        }

        public Task<Book> UpdateAsync(Guid bookId, Book bookModel)
        {
            return _bookRepository.UpdateBookAsync(bookId, bookModel);
        }

        public Task<Book> DeleteAsync(Guid bookId)
        {
            return _bookRepository.DeleteBookAsync(bookId);
        }
    }
}
