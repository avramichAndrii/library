﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Domain.Exceptions
{
    public class BookNotFoundException : DomainException
    {
        private const string NotFoundMessage = "Book info  with id = {0} is not found.";

        public BookNotFoundException() : base() { }

        public BookNotFoundException(Guid id) : base(string.Format(NotFoundMessage, id)) { }
    }
}
