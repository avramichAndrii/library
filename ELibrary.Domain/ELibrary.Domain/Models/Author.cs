﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Domain.Models
{
    public class Author
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Year { get; set; }
        public string Description { get; set; }

        public byte[] ImageData { get; set; }

        public List<Book> Books { get; set; }
    }
}
