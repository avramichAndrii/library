﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Domain.Models
{
    public class Book
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public DateTime DateOfPublishing { get; set; }

        public byte[] ImageData { get; set; }

        public Guid AuthorId { get; set; }
        public List<Comment> Comments { get; set; }


    }
}
