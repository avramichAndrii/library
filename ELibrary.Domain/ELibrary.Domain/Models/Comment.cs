﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Domain.Models
{
    public class Comment
    {
        public Guid CommentId { get; set; }

        public Guid BookId { get; set; }

        public Guid UserId { get; set; }

        public string CommentBody { get; set; }

    }
}
