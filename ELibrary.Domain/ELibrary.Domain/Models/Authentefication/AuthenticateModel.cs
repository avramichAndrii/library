﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Domain.Models.Authentefication
{
    public class AuthenticateModel
    {
        public string AccessToken { get; set; }
        public Guid RefreshToken { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}
