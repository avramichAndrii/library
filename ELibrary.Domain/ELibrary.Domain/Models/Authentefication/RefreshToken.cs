﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Domain.Models.Authentefication
{
    public class RefreshToken 
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid Token { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}
