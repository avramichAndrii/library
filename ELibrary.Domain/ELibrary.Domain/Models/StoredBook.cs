﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Domain.Models
{
   public class StoredBook
    {
        public Guid StoredId { get; set; }
        public Guid BookId { get; set; }
        public byte[] Data { get; set; }
    }
}
