﻿using ELibrary.Domain.Abstractions;

using ELibrary.Domain.Abstractions.Services;
using ELibrary.Domain.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Domain.Extensions
{
    public static class BookExtensions
    {
        public static void RegisterBookDomain(this IServiceCollection collection)
        {
            collection.AddScoped<IBookService, BookService>();
            collection.AddScoped<IAuthorService, AuthorService>();
            collection.AddScoped<ICommentService, CommentService>();
            collection.AddScoped<IStoredBookService, StoredBookService>();
        }
    }
}
