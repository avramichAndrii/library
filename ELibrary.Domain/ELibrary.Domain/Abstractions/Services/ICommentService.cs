﻿using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions.Services
{
    public interface ICommentService
    {
        Task<IReadOnlyCollection<Comment>> GetAllByBookAsync(Guid commentId);
        Task<Comment> GetBookByIdAsync(Guid commentId);
        Task<Comment> CreateAsync(Comment commentModel);
        Task<Comment> DeleteAsync(Guid commentId);
    }
}
