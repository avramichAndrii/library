﻿using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions
{
    public interface IBookService
    {
        Task<IReadOnlyCollection<Book>> GetAllAsync();

        Task<Book> GetBookByIdAsync(Guid bookId);

        Task<Book> CreateAsync(Book bookModel);

        Task<Book> UpdateAsync(Guid bookId, Book bookModel);

        Task<Book> DeleteAsync(Guid bookId);

        Task<Book> GetBookByName(string name);

    }
}
