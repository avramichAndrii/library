﻿using ELibrary.Domain.Models.Authentefication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions.Services
{
    public interface IUserService
    {
        Task<AuthenticateModel> AuthenticateAsync(string login, string password);

        Task<IReadOnlyCollection<User>> GetAllUsersAsync();
        Task<User> GetUserByIdAsync(Guid userId);
        Task<User> CreateAsync(User user);
    }
}
