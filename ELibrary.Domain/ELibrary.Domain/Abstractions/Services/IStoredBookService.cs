﻿using ELibrary.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions.Services
{
    public interface IStoredBookService
    {
        Task<StoredBook> CreateAsync(StoredBook bookModel);
        Task<StoredBook> GetByIdAsync(Guid bookId);
    }
}
