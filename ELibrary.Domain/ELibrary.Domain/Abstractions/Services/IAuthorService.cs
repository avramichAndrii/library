﻿using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions.Services
{
    public interface IAuthorService
    {
        Task<IReadOnlyCollection<Author>> GetAllAsync();

        Task<Author> GetAuthorByIdAsync(Guid authorId);

        Task<Author> CreateAsync(Author authorModel);

        Task<Author> UpdateAsync(Guid authorId, Author authorModel);

        Task<Author> DeleteAsync(Guid authorId);

    }
}
