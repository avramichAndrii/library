﻿using ELibrary.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions.Repositories
{
    public interface IStoredBookRepository
    {
        Task<StoredBook> GetBookByIdAsync(Guid bookId);
        Task<StoredBook> CreateStoredBookAsync(StoredBook storedBook);
    }
}
