﻿using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions.Repositories
{
    public interface ICommentRepository
    {
        Task<IReadOnlyCollection<Comment>> GetAllByBookAsync(Guid bookId);
        Task<Comment> GetByIdAsync(Guid commentId);
        Task<Comment> CreateCommentAsync(Comment commentModel);
        Task<Comment> DeleteCommentAsync(Guid commentId);
    }
}
