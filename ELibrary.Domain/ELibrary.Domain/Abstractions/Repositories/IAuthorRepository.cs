﻿using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions
{
    public interface IAuthorRepository
    {
        Task<IReadOnlyCollection<Author>> GetAllAsync();

        Task<Author> GetAuthorByIdAsync(Guid authorId);

        Task<Author> CreateAuthorAsync(Author authorModel);
        Task<Author> UpdateAuthorAsync(Guid authorId, Author authorModel);
        Task<Author> DeleteAuthorAsync(Guid authorId);

    }
}
