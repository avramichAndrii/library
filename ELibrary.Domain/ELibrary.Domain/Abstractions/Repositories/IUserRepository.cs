﻿using ELibrary.Domain.Models.Authentefication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions.Repositories
{
    public interface IUserRepository
    {
        Task<User> CreateAsync(User userModel);
        Task<User> DeleteAsync(Guid userId);
        Task<IReadOnlyCollection<User>> GetAllAsync();
        Task<User> GetByIdAsync(Guid userId);
        Task<User> GetUserByLogin(string login);

        Task<RefreshToken> CreateRefreshTokenAsync(RefreshToken token);

        Task DeleteRefreshTokenAsync(RefreshToken token);
    }
}
