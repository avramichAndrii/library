﻿using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Domain.Abstractions
{
    public interface IBookRepository
    {
        Task<Book> GetBooksByIdAsync(Guid bookId);

        Task<IReadOnlyCollection<Book>> GetAllAsync();
        Task<Book> CreateBookAsync(Book bookModel);
        Task<Book> UpdateBookAsync(Guid bookId, Book bookModel);
        Task<Book> DeleteBookAsync(Guid bookId);
        Task<Book> GetBookByNameAsync(string name);
    }
}
