﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.API.Requests
{
    public class AuthorRequest
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Year { get; set; }
        public string Description { get; set; }
        public byte[] ImageData { get; set; }
    }
}
