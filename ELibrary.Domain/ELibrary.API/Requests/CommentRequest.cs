﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.API.Requests
{
    public class CommentRequest
    {

        public Guid BookId { get; set; }

        public Guid UserId { get; set; }

        public string CommentBody { get; set; }
    }
}
