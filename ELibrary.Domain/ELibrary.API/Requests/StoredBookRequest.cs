﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.API.Requests
{
    public class StoredBookRequest
    {
        public Guid BookId { get; set; }
        public byte[] Data { get; set; }
    }
}
