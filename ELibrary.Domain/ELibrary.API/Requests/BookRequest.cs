﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.API.Requests
{
    public class BookRequest
    {
        public string Name { get; set; }
        public string Language { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public Guid AuthorId { get; set; }
        public byte[] ImageData { get; set; }
    }
}
