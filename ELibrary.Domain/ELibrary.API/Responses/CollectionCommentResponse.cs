﻿using ELibrary.Domain.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace ELibrary.API.Responses
{
    public class CollectionCommentResponse
    {
        public IReadOnlyCollection<Comment> CommentCollection { get; private set; }

        public CollectionCommentResponse(IList<Comment> commentsCollection)
        {
            CommentCollection = new ReadOnlyCollection<Comment>(commentsCollection);
        }
    }
}
