﻿using ELibrary.Domain.Models.Authentefication;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ELibrary.API.Responses
{
    public class CollectionUserResponse
    {
        public IReadOnlyCollection<User> UserCollection { get; private set; }

        public CollectionUserResponse(IList<User> userCollection)
        {
            UserCollection = new ReadOnlyCollection<User>(userCollection);
        }
    }
}
