﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.API.Responses
{
    public class AuthenticateResponse
    {
        public string AccessToken { get; set; }
        public Guid RefreshToken { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
}
