﻿using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ELibrary.API.Responses
{
    public class CollectionBookResponse
    {
        public IReadOnlyCollection<Book> BookCollection { get; private set; }

        public CollectionBookResponse(IList<Book> bookCollection)
        {
            BookCollection = new ReadOnlyCollection<Book>(bookCollection);
        }
    }
}
