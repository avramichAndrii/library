﻿using ELibrary.Domain.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ELibrary.API.Responses
{
    public class CollectionAuthorResponse
    {
        public IReadOnlyCollection<Author> AuthorCollection { get; private set; }

        public CollectionAuthorResponse(IList<Author> authorsCollection)
        {
            AuthorCollection = new ReadOnlyCollection<Author>(authorsCollection);
        }
    }
}
