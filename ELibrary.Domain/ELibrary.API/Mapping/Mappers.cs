﻿using ELibrary.API.Requests;
using ELibrary.API.Responses;
using ELibrary.Domain.Models;
using ELibrary.Domain.Models.Authentefication;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.API.Mapping
{
    public static class Mappers
    {
        public static Book MapFromRequest(this BookRequest bookModel)
        {
            return new Book()
            {
                Id = Guid.NewGuid(),
                Name = bookModel.Name,
                Description = bookModel.Description,
                Language = bookModel.Language,
                Year = bookModel.Year,
                DateOfPublishing = DateTime.Now,
                AuthorId = bookModel.AuthorId,
                ImageData = bookModel.ImageData  
            };
        }

        public static Author MapFromRequest(this AuthorRequest authorModel)
        {
            return new Author()
            {
                id = Guid.NewGuid(),
                Name = authorModel.Name,
                Description = authorModel.Description,
                Year = authorModel.Year,
                LastName = authorModel.LastName,
                ImageData = authorModel.ImageData,
                Books = new List<Book>()
            };
        }

        public static User MapFromRequest(this RegisterRequest registerModel)
        {
            return new User()
            {
               UserId = Guid.NewGuid(),
               FirstName = registerModel.FirstName,
               LastName = registerModel.LastName,
               Email = registerModel.Email,
               Password = registerModel.Password,
               Login = registerModel.Login,
               ImageData = registerModel.ImageData,
               Role = registerModel.Role
            };
        }

        public static Comment MapFromRequest(this CommentRequest request)
        {
            return new Comment()
            {
                CommentId = Guid.NewGuid(),
                CommentBody = request.CommentBody,
                BookId = request.BookId,
                UserId = request.UserId
            };
        }

        public static StoredBook MapGromRequest(this StoredBookRequest request)
        {
            return new StoredBook()
            {
                StoredId = Guid.NewGuid(),
                BookId = request.BookId,
                Data = request.Data
            };
        }

        public static AuthenticateResponse MapFromRequest(this AuthenticateModel authenticateModel)
        {
            return new AuthenticateResponse()
            {
                AccessToken = authenticateModel.AccessToken,
                RefreshToken = authenticateModel.RefreshToken,
                ExpiresAt = authenticateModel.ExpiresAt
            };
        }

    }
}
