﻿using ELibrary.API.Mapping;
using ELibrary.API.Requests;
using ELibrary.API.Responses;
using ELibrary.Domain.Abstractions.Services;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELibrary.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CommentsController : ControllerBase
    {
        private readonly ICommentService _commentService;

        public CommentsController(ICommentService service)
        {
            _commentService = service;
        }


        [HttpGet("{bookId}")]
        public async Task<CollectionCommentResponse> GetAllAsync(Guid bookId)
        {
            IReadOnlyCollection<Comment> comments = await _commentService.GetAllByBookAsync(bookId);
            return new CollectionCommentResponse(comments.Select(x => x).ToList());
        }


        [HttpGet("one/{commentId}")]
        public async Task<ActionResult<Comment>> GetBookByIdAsync(Guid commentId)
        {
            try
            {
                var response = await _commentService.GetBookByIdAsync(commentId);
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


        [HttpPost]
        public async Task<ActionResult<Comment>> CreateCommentAsync([FromBody] CommentRequest request)
        {
            try
            {
                var response = await _commentService.CreateAsync(request.MapFromRequest());
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{commentId}")]
        public async Task<ActionResult<Comment>> DeleteAsync(Guid commentId)
        {
            try
            {
                var response = await _commentService.DeleteAsync(commentId);
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}
