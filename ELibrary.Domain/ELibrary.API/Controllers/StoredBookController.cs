﻿using ELibrary.API.Mapping;
using ELibrary.API.Requests;
using ELibrary.Domain.Abstractions.Services;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ELibrary.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StoredBookController : ControllerBase
    {
        public readonly IStoredBookService _storedBookService;

        public StoredBookController(IStoredBookService storedBookService)
        {
            _storedBookService = storedBookService;
        }

        [HttpGet("{bookId}")]
        public async Task<ActionResult<StoredBook>> GetStoredBookAsync(Guid bookId)
        {
            try
            {
                var response = await _storedBookService.GetByIdAsync(bookId);
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("download/{bookId}")]
        public FileResult Download(Guid bookId)
        {
                var data = _storedBookService.GetByIdAsync(bookId);
                string file_type = "application/fb2";
                string file_name = "book2.fb2";
                return File(data.Result.Data, file_type, file_name);
        }

        [HttpPost]
        public async Task<ActionResult<StoredBook>> CreateStoredBookAsync([FromBody] StoredBookRequest storedBook)
        {
            try
            {
                var response = await _storedBookService.CreateAsync(storedBook.MapGromRequest());
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
