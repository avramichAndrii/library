﻿using ELibrary.API.Mapping;
using ELibrary.API.Requests;
using ELibrary.API.Responses;
using ELibrary.Domain.Abstractions.Services;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models.Authentefication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;


        public UserController(IUserService userService)
        {
            _userService = userService;

        }
        #region users
        [HttpGet]
        public async Task<ActionResult<CollectionUserResponse>> GetAllAsync()
        {
            try
            {
                var users = await _userService.GetAllUsersAsync();
                return new CollectionUserResponse(users.Select(x => x).ToList());

            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<User>> GetByIdAsync(Guid userId)
        {
            try
            {
                var user = await _userService.GetUserByIdAsync(userId);
                return user;

            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
        #endregion

        #region tokens

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateAsync([FromBody] AuthenticateRequest model)
        {
            AuthenticateModel authenticateModel = await _userService.AuthenticateAsync(model.Login, model.Password);
            if (authenticateModel == null)
            {
                return Unauthorized(new { message = "Incorrect login or password" });
            }
            return Ok(authenticateModel.MapFromRequest());
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    message = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .ToList()
                });
            }
            User user = request.MapFromRequest();

            try
            {
                var createdUser = await _userService.CreateAsync(user);
                return Ok(createdUser);
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        #endregion
    }
}
