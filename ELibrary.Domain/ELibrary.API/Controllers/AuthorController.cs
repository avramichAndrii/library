﻿using ELibrary.API.Mapping;
using ELibrary.API.Requests;
using ELibrary.API.Responses;
using ELibrary.Domain.Abstractions.Services;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }


        [HttpGet]
        public async Task<CollectionAuthorResponse> GetAllAsync()
        {
            IReadOnlyCollection<Author> authors = await _authorService.GetAllAsync();
            return new CollectionAuthorResponse(authors.Select(x => x).ToList());
        }


        [HttpGet("{authorId}")]
        public async Task<ActionResult<Author>> GetAuthorByIdAsync(Guid authorId)
        {
            try
            {
                var response = await _authorService.GetAuthorByIdAsync(authorId);
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }


        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<Author>> CreateAuthorAsync([FromBody] AuthorRequest author)
        {
            try
            {
                var response = await _authorService.CreateAsync(author.MapFromRequest());
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{bookId}")]
        public async Task<ActionResult<Author>> UpdateAsync(Guid authorId, [FromBody] Author author)
        {
            try
            {
                var response = await _authorService.UpdateAsync(authorId, author);
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{bookId}")]
        public async Task<ActionResult<Author>> DeleteAsync(Guid authorId)
        {
            try
            {
                var response = await _authorService.DeleteAsync(authorId);
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
 

    }
}
