﻿using ELibrary.API.Mapping;
using ELibrary.API.Requests;
using ELibrary.API.Responses;
using ELibrary.Domain.Abstractions;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ELibrary.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

       
        [HttpGet]
        public async Task<CollectionBookResponse> GetAllAsync()
        {
            IReadOnlyCollection<Book> books = await _bookService.GetAllAsync();
            return new CollectionBookResponse(books.Select(x => x).ToList());

        }


        [HttpGet("{bookId}")]
        public async Task<ActionResult<Book>> GetBookByIdAsync(Guid bookId)
        {
            try
            {
                var response = await _bookService.GetBookByIdAsync(bookId);
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("name/{name}")]
        public async Task<ActionResult<Book>> GetBookByNameAsync(string name)
        {
            try
            {
                var response = await _bookService.GetBookByName(name);
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        //  [Authorize(Roles = "Admin")]
        // [Authorize]
        [HttpPost]
        public async Task<ActionResult<Book>> CreateBookAsync([FromBody] BookRequest book)
        {
            try
            {
                var response = await _bookService.CreateAsync(book.MapFromRequest());
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        //[Authorize(Roles = "Admin")]
        [Authorize]
        [HttpPut("{bookId}")]
        public async Task<ActionResult<Book>> UpdateAsync(Guid bookId, [FromBody] BookRequest book)
        {
            try
            {
                var response = await _bookService.UpdateAsync(bookId, book.MapFromRequest());
                return response;
            }

            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [Authorize]    
        [HttpDelete("{bookId}")]
        public async Task<ActionResult<Book>> DeleteAsync(Guid bookId)
        {
            try
            {
                var response = await _bookService.DeleteAsync(bookId);
                return response;
            }
            catch (DomainException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}
