﻿using ELibrary.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace ELibrary.Persistence.Builders
{
    public class CommentModelBuilder : IEntityTypeConfiguration<Comment>
    {
        private const string TableName = "Comments";
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.CommentId);
            builder.Property(x => x.UserId);
            builder.Property(x => x.CommentBody);

        }

    }
}
