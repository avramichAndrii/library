﻿using ELibrary.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ELibrary.Persistence.Builders
{
    public class StoredBookModelBuilder : IEntityTypeConfiguration<StoredBook>
    {
        private const string TableName = "Stored Books";
        public void Configure(EntityTypeBuilder<StoredBook> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(z => z.StoredId);
            builder.Property(z => z.BookId);
            builder.Property(z => z.Data);
        }
    }
}
