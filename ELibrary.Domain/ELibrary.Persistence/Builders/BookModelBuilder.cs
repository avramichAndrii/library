﻿using ELibrary.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;


namespace ELibrary.Persistence.Builders
{
    class BookModelBuilder : IEntityTypeConfiguration<Book>
    {
        private const string TableName = "Books";
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name);
            builder.Property(x => x.Language);
            builder.Property(x => x.Year);
            builder.Property(x => x.Description);
            builder.Property(x => x.DateOfPublishing);
            builder.Property(x => x.ImageData);

        }
    }

}
