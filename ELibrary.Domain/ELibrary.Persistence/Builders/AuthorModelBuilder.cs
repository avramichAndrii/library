﻿using ELibrary.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ELibrary.Persistence.Builders
{
    public class AuthorModelBuilder : IEntityTypeConfiguration<Author>
    {
        private const string TableName = "Authors";
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            builder.ToTable(TableName);
            builder.HasKey(x => x.id);
            builder.Property(x => x.Name);
            builder.Property(x => x.LastName);
            builder.Property(x => x.Year);
            builder.Property(x => x.Description);
            builder.Property(x => x.ImageData);

        }

    }
}
