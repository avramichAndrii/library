﻿using ELibrary.Domain.Abstractions.Repositories;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models;
using ELibrary.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace ELibrary.Persistence.Repositories
{
    public class StoredBookRepository : IStoredBookRepository
    {
        private readonly LibraryContext _libraryContext;

        public StoredBookRepository(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }


        public async Task<StoredBook> GetBookByIdAsync(Guid bookId)
        {
            try
            {
                var book = await _libraryContext.StoredBooks.SingleOrDefaultAsync(x => x.BookId == bookId);
                return book;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }

        public async Task<StoredBook> CreateStoredBookAsync(StoredBook storedBook)
        {
            try
            {
                await _libraryContext.StoredBooks.AddAsync(storedBook);
                await _libraryContext.SaveChangesAsync();
                return storedBook;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }
    }
}
