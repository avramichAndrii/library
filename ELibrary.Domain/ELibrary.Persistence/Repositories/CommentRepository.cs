﻿using ELibrary.Domain.Abstractions.Repositories;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models;
using ELibrary.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ELibrary.Persistence.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly LibraryContext _libraryContext;
        public CommentRepository(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
        }

        public async Task<IReadOnlyCollection<Comment>> GetAllByBookAsync(Guid bookId)
        {
            try
            {
                return await _libraryContext.Comments.Where(x => x.BookId == bookId).ToListAsync();
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }

        public async Task<Comment> GetByIdAsync(Guid commentId)
        {
            try
            {
                return await _libraryContext.Comments.SingleOrDefaultAsync(x => x.CommentId == commentId);
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }

        public async Task<Comment> CreateCommentAsync(Comment commentModel)
        {
            try
            {
                await _libraryContext.Comments.AddAsync(commentModel);
                await _libraryContext.SaveChangesAsync();
                return commentModel;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }

        public async Task<Comment> DeleteCommentAsync(Guid commentId)
        {
            try
            {
                var commentToDelete = await _libraryContext.Comments.SingleOrDefaultAsync(x => x.CommentId == commentId);
                if (commentToDelete is null)
                {
                    return null;
                }
                _libraryContext.Comments.Remove(commentToDelete);
                await _libraryContext.SaveChangesAsync();
                return commentToDelete;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }
    }
}
