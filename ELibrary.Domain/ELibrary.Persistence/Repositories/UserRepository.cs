﻿using ELibrary.Domain.Abstractions.Repositories;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models.Authentefication;
using ELibrary.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ELibrary.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly LibraryContext _libraryContext;
        public UserRepository(LibraryContext context)
        {
            _libraryContext = context;
        }

        public async Task<User> CreateAsync(User userModel)
        {
            try
            {
                _libraryContext.Users.Add(userModel);
                await _libraryContext.SaveChangesAsync();
                return userModel;
            }
            catch (InvalidOperationException ioex)
            {
                throw new DomainException(ioex.Message);

            }
        }

        public async Task<User> DeleteAsync(Guid userId)
        {
            try
            {
                var userToRemove = await _libraryContext.Users.SingleOrDefaultAsync(user => user.UserId == userId);
                _libraryContext.Users.Remove(userToRemove);
                await _libraryContext.SaveChangesAsync();
                return userToRemove;
            }
            catch (InvalidOperationException)
            {
                throw new DomainException("Can`t delete");
            }
        }

        public async Task<IReadOnlyCollection<User>> GetAllAsync()
        {
            try
            {
                return await _libraryContext.Users.ToListAsync();
            }
            catch (InvalidOperationException ioex)
            {
                throw new DomainException(ioex.Message);
            }
        }

        public async Task<User> GetByIdAsync(Guid userId)
        {
            try
            {
                User user = await _libraryContext.Users.SingleOrDefaultAsync(x => x.UserId == userId);
                if (user == null)
                {
                    throw new DomainException("Not found by id");
                }
                return user;
            }
            catch (InvalidOperationException)
            {
                throw new DomainException("Not found by id");
            }
        }

        public async Task<User> GetUserByLogin(string login)
        {
            try
            {
                User user = await _libraryContext.Users.SingleOrDefaultAsync(x => x.Login == login);
                if (user == null)
                {
                    throw new DomainException("Not found by login");
                }
                return user;
            }
            catch (InvalidOperationException)
            {
                throw new DomainException("Not found by login");
            }
        }

        public async Task<RefreshToken> CreateRefreshTokenAsync(RefreshToken token)
        {
            try
            {
                await DeleteRefreshTokenAsync(token);
                _libraryContext.RefreshTokens.Add(token);
                await _libraryContext.SaveChangesAsync();
                return token;
            }
            catch (InvalidOperationException ioex)
            {
                throw new DomainException(ioex.Message);

            }
        }

        public async Task DeleteRefreshTokenAsync(RefreshToken token)
        {
            try
            {
                var oldToken = await _libraryContext.RefreshTokens.SingleOrDefaultAsync(rt => rt.UserId == token.UserId);
                if (oldToken != null)
                {
                    _libraryContext.RefreshTokens.Remove(oldToken);
                    await _libraryContext.SaveChangesAsync();
                }
            }
            catch (InvalidOperationException ioex)
            {
                throw new DomainException(ioex.Message);
            }
        }
    }
}
