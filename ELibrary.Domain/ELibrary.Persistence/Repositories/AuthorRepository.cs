﻿using ELibrary.Domain.Abstractions;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models;
using ELibrary.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Persistence.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly LibraryContext _libraryContext;
        public AuthorRepository(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;

        }

        public IIncludableQueryable<Author, List<Book>> IncludeBookList()
        {
            return _libraryContext.Authors.Include(model => model.Books);
        }

        public async Task<IReadOnlyCollection<Author>> GetAllAsync()
        {
            try
            {
                return await IncludeBookList().ToListAsync();
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }
        public async Task<Author> GetAuthorByIdAsync(Guid authorId)
        {
            var authorModel = await IncludeBookList().SingleOrDefaultAsync(x => x.id == authorId);
            if (authorModel == null)
            {
                throw new BookNotFoundException(authorId);
            }
            return authorModel;
        }

        public async Task<Author> CreateAuthorAsync(Author authorModel)
        {
            try
            {
                _libraryContext.Authors.Add(authorModel);
                await _libraryContext.SaveChangesAsync();
                return authorModel;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }

        public async Task<Author> UpdateAuthorAsync(Guid authorId, Author authorModel)
        {
            try
            {
                Author modelToUpdate = await IncludeBookList().SingleOrDefaultAsync(x => x.id == authorId);
                if (modelToUpdate == null)
                {
                    throw new BookNotFoundException(authorId);
                }
                modelToUpdate.Name = authorModel.Name;
                modelToUpdate.LastName = authorModel.LastName;
                modelToUpdate.Year = authorModel.Year;
                modelToUpdate.Description = authorModel.Description;
                modelToUpdate.Books = authorModel.Books;
                modelToUpdate.ImageData = authorModel.ImageData;
                
                await _libraryContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }

        public async Task<Author> DeleteAuthorAsync(Guid authorId)
        {
            try
            {
                Author author = await IncludeBookList().SingleOrDefaultAsync(x => x.id == authorId);
                if (author == null)
                {
                    throw new BookNotFoundException(authorId);
                }
                _libraryContext.Authors.Remove(author);
                await _libraryContext.SaveChangesAsync();
                return author;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }
    }
}
