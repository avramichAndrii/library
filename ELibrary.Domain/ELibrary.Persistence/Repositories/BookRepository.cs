﻿using ELibrary.Domain.Abstractions;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models;
using ELibrary.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Persistence.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly LibraryContext _libraryContext;
        public BookRepository(LibraryContext libraryContext)
        {
            _libraryContext = libraryContext;
            
        }

        public async Task<IReadOnlyCollection<Book>> GetAllAsync()
        {
            try
            {
                return await _libraryContext.BookModels.ToListAsync();
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }
        public async Task<Book> GetBooksByIdAsync(Guid bookId)
        {
            var bookModel = await _libraryContext.BookModels.SingleOrDefaultAsync(x => x.Id == bookId);
            if (bookModel == null)
            {
                throw new BookNotFoundException(bookId);
            }
            return bookModel;
        }

        public async Task<Book> GetBookByNameAsync(string name)
        {
           
            var model = await _libraryContext.BookModels.SingleOrDefaultAsync(z => z.Name == name);
            if (model == null)
            {
                throw new BookNotFoundException();
            }
            return model;

        }

        public async Task<Book>  CreateBookAsync(Book bookModel)
        {
            try
            {
                _libraryContext.BookModels.Add(bookModel);
                await _libraryContext.SaveChangesAsync();
                return bookModel;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }

        public async Task<Book> UpdateBookAsync(Guid bookId,Book bookModel)
        {
            try
            {
                Book modelToUpdate = await _libraryContext.BookModels.SingleOrDefaultAsync(x => x.Id == bookId);
                if (modelToUpdate == null)
                {
                    throw new BookNotFoundException(bookId);
                }
                modelToUpdate.Name = bookModel.Name;
                modelToUpdate.Language = bookModel.Language;
                modelToUpdate.Year = bookModel.Year;
                modelToUpdate.Description = bookModel.Description;
                modelToUpdate.DateOfPublishing = bookModel.DateOfPublishing;
                await _libraryContext.SaveChangesAsync();
                return modelToUpdate;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }

        public async Task<Book> DeleteBookAsync(Guid bookId)
        {
            try
            {
                Book book = await _libraryContext.BookModels.SingleOrDefaultAsync(x => x.Id == bookId);
                if (book == null)
                {
                    throw new BookNotFoundException(bookId);
                }
                _libraryContext.BookModels.Remove(book);
                await _libraryContext.SaveChangesAsync();
                return book;
            }
            catch (InvalidOperationException ex)
            {
                throw new DomainException(ex.Message);
            }
        }
    }
}
