﻿using ELibrary.Domain.Models;
using ELibrary.Domain.Models.Authentefication;
using ELibrary.Persistence.Builders;
using Microsoft.EntityFrameworkCore;

namespace ELibrary.Persistence.Context
{
    public sealed class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Book> BookModels { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<StoredBook> StoredBooks { get; set; }

        public DbSet<RefreshToken> RefreshTokens { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<Book>(new BookModelBuilder());
            modelBuilder.ApplyConfiguration<Author>(new AuthorModelBuilder());
            modelBuilder.ApplyConfiguration<User>(new UserModelBuilder());
            modelBuilder.ApplyConfiguration<Comment>(new CommentModelBuilder());
            modelBuilder.ApplyConfiguration<StoredBook>(new StoredBookModelBuilder());
            base.OnModelCreating(modelBuilder);
        }
    }
}

