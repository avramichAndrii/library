﻿using ELibrary.Persistence.Context;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using ELibrary.Domain.Abstractions;
using ELibrary.Persistence.Repositories;
using ELibrary.Domain.Abstractions.Repositories;

namespace ELibrary.Persistence.ContextDI
{


    public static class LibraryContextDIExtension
    {
        private const string DBConnectionString = nameof(DBConnectionString);
        public static void RegisterLibraryContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString(DBConnectionString);
            services.AddDbContextPool<LibraryContext>(option => option.UseMySql(connection));
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<IAuthorRepository, AuthorRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddScoped<IStoredBookRepository, StoredBookRepository>();
          
        }
    }
}