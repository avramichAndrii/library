﻿using ELibrary.Application.Helpers;
using ELibrary.Domain.Abstractions.Repositories;
using ELibrary.Domain.Abstractions.Services;
using ELibrary.Domain.Exceptions;
using ELibrary.Domain.Models.Authentefication;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ELibrary.Application
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        
        private readonly RefreshTokenSettings _refreshTokenSettings;
        private readonly AuthenticationSetting _authSettings;

        public UserService(IUserRepository userRepository,
            IOptions<RefreshTokenSettings> refreshTokenSettings,
             IOptions<AuthenticationSetting> authSettings)
        {
            _authSettings = authSettings.Value;
            _userRepository = userRepository;
            _refreshTokenSettings = refreshTokenSettings.Value;
        }

        public async Task<AuthenticateModel> AuthenticateAsync(string login,string password)
        {
            var user = await _userRepository.GetUserByLogin(login);
            if(user is null)
            {
                return null;
            }
            try
            {
                var refreshToken = JwtTokenProvider.GenerateRefreshToken(user.UserId, _refreshTokenSettings);
                await _userRepository.CreateRefreshTokenAsync(refreshToken);
                return new AuthenticateModel
                {
                    AccessToken = JwtTokenProvider.GenerateAccessToken(_authSettings, user),
                    ExpiresAt = refreshToken.ExpiresAt,
                    RefreshToken = refreshToken.Token
                };
            }
            catch (ArgumentNullException anex)
            {
                throw new DomainException(anex.Message);
            }
            catch (ArgumentException aex)
            {
                throw new DomainException(aex.Message);
            }
        }

        public Task<IReadOnlyCollection<User>> GetAllUsersAsync()
        {
            return _userRepository.GetAllAsync();
        }

        public Task<User> GetUserByIdAsync(Guid userId)
        {
            return _userRepository.GetByIdAsync(userId);    
        }

        public Task<User> CreateAsync(User user)
        {
            return _userRepository.CreateAsync(user);
        }

    }
}
