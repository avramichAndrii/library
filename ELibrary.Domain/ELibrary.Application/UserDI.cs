﻿using ELibrary.Domain.Abstractions.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Application
{
    public static class UserDI
    {
        public static void RegisterUserDomain(this IServiceCollection collection)
        {
            collection.AddScoped<IUserService, UserService>();

        }
    }
}
