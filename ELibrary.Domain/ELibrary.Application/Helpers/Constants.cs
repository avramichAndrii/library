﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Application.Helpers
{
    public static class Constants
    {
        public const string Audiance = "http://localhost:5001/";
        public const string Issuer = Audiance;
        public const string Secret = "some_secret_for_library";
    }
}
