﻿using ELibrary.Domain.Models.Authentefication;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Authentication;
using System.Security.Claims;
using System.Text;

namespace ELibrary.Application.Helpers
{
    public class JwtTokenProvider
    {
        public static string GenerateAccessToken(AuthenticationSetting authenticationSetting,User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var secretBytes = Encoding.UTF8.GetBytes(Constants.Secret);
            var key = new SymmetricSecurityKey(secretBytes);
            var algorithm = SecurityAlgorithms.HmacSha256;
            var signingCreedentials = new SigningCredentials(key, algorithm);

            SecurityTokenDescriptor securityToken = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.UserId.ToString()),
                    new Claim(ClaimTypes.Role,user.Role.ToString())
                }),
                Issuer = authenticationSetting.Issuer,
                Audience = authenticationSetting.Audience,
                Expires = DateTime.UtcNow.AddMinutes(Int32.Parse(authenticationSetting.JwtLifetime)),
                SigningCredentials = signingCreedentials
            };
            var token = tokenHandler.CreateToken(securityToken);
            return tokenHandler.WriteToken(token);
        }
        public static RefreshToken GenerateRefreshToken(Guid userId, RefreshTokenSettings refreshTokenSettings)
        {
            return new RefreshToken
            {
                Token = Guid.NewGuid(),
                ExpiresAt = DateTime.UtcNow.AddDays(Int32.Parse(refreshTokenSettings.Lifetime)),
                UserId = userId
            };
        }
    }
}
