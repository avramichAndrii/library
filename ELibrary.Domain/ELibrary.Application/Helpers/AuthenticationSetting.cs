﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Application.Helpers
{
    public class AuthenticationSetting
    {
        public string Secret { get; set; }
        public string JwtLifetime { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
    }
}
