﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ELibrary.Application.Helpers
{
    public class RefreshTokenSettings
    {
        public string Lifetime { get; set; }
    }
}
